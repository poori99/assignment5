#include <stdio.h>

int income(int,int);
int expenditure(int);
int profit(int,int);
int x,y;

int income(int x, int y)
{
  return x*y;
}

int expenditure(int y)
{
  return 500+3*y;
}

int profit(int x, int y)
{
  return income(x,y)-expenditure(y);
}

int main()
{
  printf("%d\n", profit(10,140));
  return 0;
}
